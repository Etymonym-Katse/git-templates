#!/bin/sh
pushd ~/.git-templates
./copyReadme.sh $1 $2
./copyLicense.sh $1 $3
popd
