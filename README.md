# Git Templates
This is where I store a handful of templating files / helper scripts to make setting up a Git Repo easier. I also am using this repo as a means of practicing with bash scripts, git repos, and the relevant boilerplate.
## Usage
If you would like to use this project, there are two main scripts at the moment that you may like to use together -- which there is a third script for --  or individually. The scripts are configured so that they can be moved from this repo while still pointing back to it and the templates contained therein. So if you have somewhere you like to put helper scripts you can put them there without having to also store the actual templates and licenses in the same location.
### [Automatically Copying a Readme](copyReadme.sh)
 - Below are descriptions of the positional arguments for [copyReadme.sh](copyReadme.sh):
 1. The first positional argument for this script is going to be the parent directory you'd like the readme to end up in. 
  - E.g. if you were setting up a repo in your `home` directory it may look something like `/home/your-username/repo-in-question`
 2. The second positional argument for this script is which readme you'd like to copy. 
  - E.g. the one used for this repo: [use-lic.md](use-lic.md), which is the name for the readme template that has the additional Usage ( you're here now! ) and License sections.
### [Automatically Copying a License](copyLicense.sh)
 - Below are -- again -- descriptions of the positional arguments for [copyLicense.sh](copyLicense.sh):
 1. The first positional argument for this script -- exactly like in the first script -- is the parent directory you'd like the license to end up in.
  - E.g. if you were setting up a repo in your `home` directory it may look something like `/home/your-username/repo-in-question`
 2. The second positional argument for this script is -- similarly to the first script -- your choice of license you'd like to copy. At present most Creative Commons Licenses are available by their code as specified [here](https://creativecommons.org/about/cclicenses/). ( The only change being instead of a space in the file names, another `-` is used, e.g. `cc-by` instead of `cc by`.
  - E.g. the one used for this repo: [cc-by-nc-sa.txt](cc-by-nc-sa.txt), which is the Creative Commons License that allows for Remixing and Redistribution while requiring Attribution to the Creator, Non-Commercial use only, and that all Adaptiations be shared under the same terms ( i.e. under the [CC BY-NC-SA license](https://creativecommons.org/licenses/by-nc-sa/4.0/) ).
### [Automatically Copying a Readme *and* License](copyBoth.sh)
 - Just as before, the descriptions for positional arguments for [copyBoth.sh](copyBoth.sh) are below:
 1. The first positional argument for this script is exactly like in the previous scripts.
 2. The second positional argument for this script will be the desired [readme](readmes/) template.
 3. The third positional argument for this script must then be the desired [license](licenses/).
## License
This repository and its contents are licensed under [this license](LICENSE.txt).
