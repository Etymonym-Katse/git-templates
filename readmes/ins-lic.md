# Repo Name
This is where I store the things that goes in this repo! I use it for things and such, as you might imagine.

## Inspiration
The list below is an incomplete record of sources of inspiration for the contents of this repo -- it will likely grow:
### [First Inspiration](https://first-inspiration-url.net)
 - Blurb about first inspiration
### [Second Inspiration](https://second-inspiration-url.net)
 - Blurb about second inspiration
## License
This repository and its contents are licensed under [this license](LICENSE.txt).
